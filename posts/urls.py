# from django.urls import path
# from .views import PostList, PostDetail, UserList, UserDetail NO NEED WITH ROUTES
# routers work directly with the viewsets to automatically generate URL patterns for us
from rest_framework.routers import SimpleRouter
# new with routers
from .views import UserViewSet, PostViewSet

router = SimpleRouter()
router.register('users', UserViewSet, basename='users')
router.register('', PostViewSet, basename='posts')
urlpatterns = router.urls
# urlpatterns = [
#         path('<int:pk>/', PostDetail.as_view()),
#         path('', PostList.as_view()),
#         path('users/', UserList.as_view()), #new
#         path('users/<int:pk>/', UserDetail.as_view()), #new

#         ]
