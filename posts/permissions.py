# Internally, Django REST Framework relies on a
# BasePermission class from which all other
# permission dasses inherit. That means the built-in
# permissions settings like Al lowAny, IsAuthenti cated,
# and others extend it.

from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # read only allowed for all
        if request.method in permissions.SAFE_METHODS:
            return True
        # write permiss is only for the authoe
        return obj.author == request.user
