# from rest_framework import generics # we don't need this if using viewsets
# permissions #permiss to limit who can view and interact with specific blog post
# from django.shortcuts import render
from .models import Post
from .serializers import PostSerializer, UserSerializer  # user is new
from .permissions import IsAuthorOrReadOnly  # new
from django.contrib.auth import get_user_model  # import with new serializers
# a viewset is a way to combine the logic for multuple related views into a single class
from rest_framework import viewsets


# all bellow is replaced with this
class PostViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


# # Create your views here.
# class PostList(generics.ListCreateAPIView):
#     #set permission after importing permission-we will set this on project level now
#     #permission_classes = (permissions.IsAuthenticated,)
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer

# class PostDetail(generics.RetrieveUpdateDestroyAPIView):
#     #set permission after importing permission
#     #permission_classes = (permissions.IsAuthenticated,)
#     permission_classes = (IsAuthorOrReadOnly,) #new
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer

# class UserList(generics.ListCreateAPIView): #new with user serializer
#     queryset = get_user_model().objects.all()
#     serializer_class = UserSerializer

# class UserDetail(generics.RetrieveUpdateDestroyAPIView): ##newnew
#     queryset = get_user_model().objects.all()
#     serializer_class = UserSerializer
#     print(queryset[2].id)
